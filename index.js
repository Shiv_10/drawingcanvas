const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

users={}

io.on('connection',(socket)=>{
  

  socket.on('new user',(name)=>{
    console.log("user connected");
    users[socket.id]=name;
    console.log(users);
  })

  socket.on('draw',(data)=>{
    // console.log(users[socket.id]+' drew');
    // console.log(data.x);
    // console.log(data.y);
    socket.broadcast.emit('draw',data);
  })

  socket.on('stop',()=>{
    socket.broadcast.emit('stop');
  })


  socket.on('disconnect',()=>{
    console.log('disconnected');
    delete (users[socket.id]);
  })
});

http.listen(port, () => console.log('listening on port ' + port));