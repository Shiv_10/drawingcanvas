window.addEventListener('load',()=>{
    const canvas= document.getElementById('canvas');
    const ctx = canvas.getContext("2d");
    const socket = io();
    var name=prompt('Enter your name!');
    users = {}


    socket.emit('new user',name);
    

    canvas.height = window.innerHeight;
    canvas.width = window.innerWidth;

    let painting = false;

    function startPosition(e){
      painting= true;
      draw(e);
    }

    function finishedPosition(){
      painting=false;
      ctx.beginPath();
      socket.emit('stop');
    }

    function draw(e){
      if(!painting) return;

      ctx.lineWidth = 5;
      ctx.lineCap = 'round';

      ctx.lineTo(e.clientX,e.clientY);
      ctx.stroke();
      ctx.beginPath();
      ctx.moveTo(e.clientX,e.clientY);

      socket.emit('draw',{x:e.clientX,y:e.clientY});

    }

    socket.on('draw',(data)=>{
      ctx.lineWidth = 5;
      ctx.lineCap = 'round';

      ctx.lineTo(data.x,data.y);
      ctx.stroke();
      ctx.beginPath();
      ctx.moveTo(data.x,data.y);
    })

    socket.on('stop',()=>{
      ctx.beginPath();
    });

    canvas.addEventListener('mousedown',startPosition);
    canvas.addEventListener('mouseup',finishedPosition);
    canvas.addEventListener('mousemove',draw);
})